-- Performance Tests on various MonadLogic implementations.
-- (1) ListT m
--   "ListT done right" version, not transformers'.
-- (2) Seq
--   Data.Sequence.Seq is already an MonadPlus. It can easily be added
--   the MonadLogic operation.
-- (3) Orig.LogicT m
--   The original LogicT
-- (4) My.LogicT m
--   Modified to support efficient interleave operation.
-- (5) SeqT m (from logict-sequence) 
{-# LANGUAGE RankNTypes                 #-}
module Main where

import           Control.Applicative
import           Control.Monad
import           Control.Monad.Identity
import           Control.Monad.ST

import           Data.Tree

import qualified Control.Monad.Logic as Orig
import           Control.Monad.Logic (MonadLogic(..))
import           ListT
import qualified LogicT as My

import qualified Data.Foldable          as F
import           Data.Sequence          (Seq, ViewL (..))
import qualified Data.Sequence          as Seq

import qualified Control.Monad.Logic.Sequence as LSeq

import           Criterion.Main

------------------------------------------------------------------------
-- make Seq an instance of MonadLogic using viewl
instance MonadLogic Seq where
  msplit s = case Seq.viewl s of
               EmptyL  -> return Nothing
               a :< as -> return (Just (a, as))

------------------------------------------------------------------------
-- how to run MonadLogic instances

-- | [a].
runList :: [a] -> [a]
runList = id

-- | ListT. Most basic Backtracking monad.
runListT_I :: ListT Identity a -> [a]
runListT_I = F.toList

-- | ListT ST.
runListT_S :: (forall s. ListT (ST s) a) -> [a]
runListT_S ma = runST (toList ma)

-- | Seq. Asymptotically fast but constants are large. No transformer version.
runContainersSeq :: Seq a -> [a]
runContainersSeq = F.toList

-- | Logic. Very fast Monad/MonadPlus operation. Slow interleave.
runOrigLogic :: Orig.Logic a -> [a]
runOrigLogic = Orig.observeAll

runOrigLogicT_S :: (forall s. Orig.LogicT (ST s) a) -> [a]
runOrigLogicT_S ma = runST (Orig.observeAllT ma)

-- | Logic. Modified version.
runMyLogic :: My.Logic a -> [a]
runMyLogic = My.observeAll

runMyLogicT_S :: (forall s. My.LogicT (ST s) a) -> [a]
runMyLogicT_S ma = runST (My.observeAllT ma)

-- | SeqT from logict-sequence
runLSeq :: LSeq.Seq a -> [a]
runLSeq = LSeq.observeAll

runLSeqT_S :: (forall s. LSeq.SeqT (ST s) a) -> [a]
runLSeqT_S ma = runST (LSeq.observeAllT ma)

------------------------------------------------------------------------
-- Measured codes
heavy_right_assoc :: (MonadLogic m) => Int -> m ()
heavy_right_assoc n = heavy >>= guard
  where
    heavy = foldr mplus (return True) (replicate n (return False))

heavy_left_assoc :: (MonadLogic m) => Int -> m ()
heavy_left_assoc n = heavy >>= guard
  where
    falses = F.foldl' mplus mzero (replicate n (return False))
    heavy = falses `mplus` return True

heavy_treelike :: (MonadLogic m) => Int -> m ()
heavy_treelike n = go n True >>= guard
  where
    go k b
      | k <= 1    = return b
      | otherwise =
          let r = k `div` 2
              l = k - r
          in go l False `mplus` go r b

heavy_interleave :: (MonadLogic m) => Int -> m ()
heavy_interleave n = interleave heavy heavy >>= guard
  where
    m = n `div` 2
    heavy = foldr mplus (return True) (replicate m (return False))

heavy_fairbind :: (MonadLogic m) => Int -> m ()
heavy_fairbind n = heavy >>= guard
  where
    m = n `div` 5
    as = [1..5] :: [Int]
    heavy = choose as >>- \k ->
            foldr mplus (return (k==5)) (replicate m (return False))

-- Copied from post by u/dagit on:
--   https://www.reddit.com/r/haskell/comments/onwfr2/logictsequence_logict_empowered_by_reflection/
makeTree :: Int -> Tree Int
makeTree n = go 0
  where
    go k = Node k (go <$> filter (< n) [k * 3 + 1, k * 3 + 2, k * 3 + 3])

bfs :: MonadLogic m => Tree a -> m a
bfs t = go (pure t)
  where
  go q = do
    mb <- msplit q
    case mb of
     Nothing -> empty
     Just (m, qs) -> pure (rootLabel m) <|> go (qs <|> choose (subForest m))

heavy_bfs :: (MonadLogic m) => Int -> m ()
heavy_bfs n = bfs (makeTree n) >>= \k -> guard (k == n)

------------------------------------------------------------------------
-- Benchmark definition
main :: IO ()
main = defaultMain
  [ bgroup "right_assoc" (forEachMonad heavy_right_assoc)
  , bgroup "left_assoc"  (forEachMonad heavy_left_assoc)
  , bgroup "treelike"    (forEachMonad heavy_treelike)
  , bgroup "interleave"  (forEachMonad heavy_interleave)
  , bgroup "fairbind"    (forEachMonad heavy_fairbind)
  , bgroup "bfs"         (forEachMonad heavy_bfs)
  ]

forEachMonad :: (forall m. Int -> (MonadLogic m) => m ()) -> [Benchmark]
forEachMonad targetLogic =
  [ bgroup "[]"      (forEachSize $ nf (runList . targetLogic))
  , bgroup "Seq"     (forEachSize $ nf (runContainersSeq . targetLogic))
  , bgroup "ListT_I" (forEachSize $ nf (runListT_I . targetLogic))
  , bgroup "ListT_S" (forEachSize $ nf (\n -> runListT_S (targetLogic n)))
  , bgroup "OrigLogic"    (forEachSize $ nf (runOrigLogic . targetLogic))
  , bgroup "OrigLogicT_S" (forEachSize $ nf (\n -> runOrigLogicT_S (targetLogic n)))
  , bgroup "MyLogic"      (forEachSize $ nf (runMyLogic . targetLogic))
  , bgroup "MyLogicT_S"   (forEachSize $ nf (\n -> runMyLogicT_S (targetLogic n)))
  , bgroup "LSeq"         (forEachSize $ nf (\n -> runLSeq (targetLogic n)))
  , bgroup "LSeqT_S"      (forEachSize $ nf (\n -> runLSeqT_S (targetLogic n)))
  ]
  where

forEachSize :: (Int -> Benchmarkable) -> [Benchmark]
forEachSize f =
  map (\n -> bench (show n) $ f n) $
    [300, 1000, 3000, 10000]
