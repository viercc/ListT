# Notice

Added [logict-sequence](https://www.reddit.com/r/haskell/comments/onwfr2/logictsequence_logict_empowered_by_reflection/)
as one of compared implementations.

# Yet Another "ListT done right" implementation

While benchmarking "ListT done right", I noticed the `MonadLogic`
instance of `LogicT` from [logict](http://hackage.haskell.org/package/logict) has
inefficient implementation of `interleave`. The following table is the time took to
consume all elements of `interleave as as`, where `as` is a list of length N/2.

|N (Length of the list)|[]     |ListT Identity|Logic  |My.LogicT Identity|
|----------------------|-------|--------------|-------|------------------|
|300                   |6.47 us|22.9 us       |1.17 ms|22.9 us           |
|1000                  |21.3 us|77.3 us       |14.9 ms|102 us            |
|3000                  |62.8 us|247 us        |161 ms |327 us            |
|10000                 |203 us |885 us        |3.28 s |1.09 ms           |

`interleave` for `LogicT` is quadratic in length of the lists. `My.LogicT` is an alternative implementation I made. Its `interleave` is implemented by first converting to `ListT`, then perform `interleave` on `ListT` and convert back the result to `LogicT`.

