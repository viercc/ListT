{-# LANGUAGE LambdaCase #-}
-- | This module is intended to be imported qualified, to avoid name clashing with
--   Prelude functions.
module ListT.Operation(
  cons, nil, append,
  repeat, zip, zipWith, zip3, zipWith3,
  iterate, unfoldr, choose, chooseM, concat,
  reduce, consume,
  take, drop, takeWhile, dropWhile, filter
) where

import           Prelude             hiding (concat, drop, dropWhile, filter,
                                      iterate, repeat, take, takeWhile, zip,
                                      zip3, zipWith, zipWith3)

import           Control.Applicative

import           ListT

-- * List operations
cons :: (Functor m) => m a -> ListT m a -> ListT m a
cons mx mxs = ListT $ fmap (`Cons` mxs) mx

nil :: (Monad m) => ListT m a
nil = empty

append :: (Monad m) => ListT m a -> ListT m a -> ListT m a
append = (<|>)

repeat :: (Functor m) => m a -> ListT m a
repeat m = let go = cons m go in go

zip :: (Applicative m) => ListT m a -> ListT m b -> ListT m (a,b)
zip = zipWith (,)

zipWith :: (Applicative m) => (a -> b -> c) -> ListT m a -> ListT m b -> ListT m c
zipWith f = go where
  go (ListT ma) (ListT mb) = ListT $ goF <$> ma <*> mb
  goF Nil _                   = Nil
  goF _ Nil                   = Nil
  goF (Cons a as) (Cons b bs) = Cons (f a b) (go as bs)

zip3 :: (Applicative m) => ListT m a -> ListT m b -> ListT m c -> ListT m (a,b,c)
zip3 = zipWith3 (,,)

zipWith3 :: (Applicative m) =>
  (a -> b -> c -> d) -> ListT m a -> ListT m b -> ListT m c -> ListT m d
zipWith3 f = go where
  go (ListT ma) (ListT mb) (ListT mc) = ListT $ goF <$> ma <*> mb <*> mc
  goF Nil _ _                             = Nil
  goF _ Nil _                             = Nil
  goF _ _ Nil                             = Nil
  goF (Cons a as) (Cons b bs) (Cons c cs) = Cons (f a b c) (go as bs cs)

iterate :: (Monad m) => (a -> m a) -> m a -> ListT m a
iterate f mx = ListT $ mx >>= \x -> return (Cons x (iterate f (f x)))

unfoldr :: (Monad m) => (s -> m (Maybe (a, s))) -> s -> ListT m a
unfoldr f s =
  ListT $ f s >>= \case
    Nothing      -> return Nil
    Just (a, s') -> return (Cons a (unfoldr f s'))

-- | Flatten ListT-of-list. Similar \"Flatten\" functions are covered
--   by different names.
--
-- Flatten ListT-of-ListT: 'join' from "Control.Monad"
-- > join :: (Monad m) => ListT m (ListT m a) -> ListT m a
-- Flatten List-of-ListT: 'msum' from "Data.Foldable"
-- > msum :: (Monad m) => [ListT m a] -> ListT m a
concat :: (Monad m) => ListT m [a] -> ListT m a
concat = (>>= choose)

-- | Monadic strict left fold on ListT.
reduce :: (Monad m) => (a -> b -> m a) -> a -> ListT m b -> m a
reduce f = loop
  where
    loop a bs = runListT bs >>= \case
      Nil        -> return a
      Cons b bs' -> f a b >>= \a' -> a' `seq` loop a' bs'

-- | Consume every element iteratively. With base Monad with specialized '>>'
--   implementation, this can be more efficient than 'reduce' with ignored accumulator.
consume :: (Monad m) => (a -> m ()) -> ListT m a -> m ()
consume f = loop
  where
    loop as = runListT as >>= \case
      Nil -> return ()
      Cons a as' -> f a >> loop as'

take :: (Monad m) => Int-> ListT m a -> ListT m a
take n m
  | n <= 0 = nil
  | otherwise =
      ListT $ runListT m >>= \case
        Nil     -> return Nil
        Cons x xs -> return $ Cons x (take (n-1) xs)

drop :: (Monad m) => Int -> ListT m a -> ListT m a
drop n m
  | n <= 0 = m
  | otherwise =
      ListT $ runListT m >>= \case
        Nil     -> return Nil
        Cons _ xs -> runListT $ drop (n-1) xs

takeWhile :: (Monad m) => (a -> m Bool) -> ListT m a -> ListT m a
takeWhile predicate = go where
  go m = ListT $ runListT m >>= \case
           Nil     -> return Nil
           Cons x xs -> predicate x >>= \case
             True  -> return $ Cons x (go xs)
             False -> return Nil

dropWhile :: (Monad m) => (a -> m Bool) -> ListT m a -> ListT m a
dropWhile predicate = go where
  go m = ListT $ runListT m >>= \case
           Nil     -> return Nil
           Cons x xs -> predicate x >>= \case
             True  -> runListT $ go xs
             False -> return $ Cons x xs

filter :: (Monad m) => (a -> m Bool) -> ListT m a -> ListT m a
filter predicate = go where
  go m = ListT $ runListT m >>= \case
           Nil -> return Nil
           Cons x xs -> predicate x >>= \case
             True -> return $ Cons x (go xs)
             False -> runListT (go xs)
