{- |
This code is based on HaskellWiki "ListT done right alternative" page:
    https://wiki.haskell.org/ListT_done_right_alternative
-}
{-# LANGUAGE DeriveFoldable        #-}
{-# LANGUAGE DeriveFunctor         #-}
{-# LANGUAGE DeriveTraversable     #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE UndecidableInstances  #-}
module ListT(
  ListT(..),
  ListF(..),
  foldListT,
  toList,
  
  choose, chooseM,
  toListLazily
) where

import           Data.Bifunctor
import           Data.Semigroup

import           Data.Functor.Classes
import           Text.Show               (showListWith)

import           Control.Monad.Cont
import           Control.Monad.Except
import           Control.Monad.Reader
import           Control.Monad.State

import           Control.Monad.ST
import           Control.Monad.ST.Unsafe (unsafeInterleaveST)

import           Control.Applicative
import           Control.Monad.Logic

newtype ListT m a = ListT { runListT :: m (ListF a (ListT m a)) }

data ListF a r = Nil | Cons a r
  deriving (Show, Read, Eq, Ord, Functor, Foldable, Traversable)

instance Bifunctor ListF where
  bimap _ _ Nil        = Nil
  bimap f g (Cons a b) = Cons (f a) (g b)

listFToMaybe :: ListF a r -> Maybe (a, r)
listFToMaybe Nil        = Nothing
listFToMaybe (Cons a r) = Just (a, r)

foldListT :: Monad m => (a -> m b -> m b) -> m b -> ListT m a -> m b
foldListT c n = go
  where
    go m = runListT m >>= \case
             Nil       -> n
             Cons x m' -> c x (go m')

toList :: (Monad m) => ListT m a -> m [a]
toList = foldListT (\a as -> fmap (a:) as) (return [])

instance Eq1 m => Eq1 (ListT m) where
  liftEq eq = go where
    go (ListT ma) (ListT mb) = liftEq goF ma mb
    goF Nil Nil                 = True
    goF (Cons a as) (Cons b bs) = eq a b && go as bs
    goF _ _                     = False

instance (Eq1 m, Eq a) => Eq (ListT m a) where
  (==) = eq1

instance Ord1 m => Ord1 (ListT m) where
  liftCompare cmp = go where
    go (ListT ma) (ListT mb) = liftCompare goF ma mb
    goF Nil Nil                 = EQ
    goF Nil (Cons _ _)          = LT
    goF (Cons _ _) Nil          = GT
    goF (Cons a as) (Cons b bs) = cmp a b <> go as bs

instance (Ord1 m, Ord a) => Ord (ListT m a) where
  compare = compare1

instance Show1 m => Show1 (ListT m) where
  liftShowsPrec showsP _ = go
    where
      go p (ListT ma) = showsUnaryWith (liftShowsPrec goF goFL) "ListT" p ma
      goF _ Nil = ("Nil" ++)
      goF p (Cons a as) = showsBinaryWith showsP go "Cons" p a as
      goFL = showListWith (goF 0)

instance (Show1 m, Show a) => Show (ListT m a) where
  showsPrec = showsPrec1

instance Functor m => Functor (ListT m) where
  fmap f = go
    where go (ListT m) = ListT $ fmap (bimap f go) m

instance Foldable m => Foldable (ListT m) where
  foldMap f = go
    where
      go (ListT mas) = foldMap goF mas

      goF Nil         = mempty
      goF (Cons a as) = f a `mappend` go as

instance Traversable m => Traversable (ListT m) where
  traverse f = go
    where
      go (ListT mas) = ListT <$> traverse goF mas
      
      goF Nil         = pure Nil
      goF (Cons a as) = Cons <$> f a <*> go as

instance Monad m => Applicative (ListT m) where
  pure = return
  (<*>) = ap

instance Monad m => Monad (ListT m) where
  return x = ListT . return $ Cons x empty
  ma >>= f = ListT $
    runListT ma >>= \case
      Nil -> return Nil
      Cons a ma' -> runListT (f a <|> (ma' >>= f))

instance MonadTrans ListT where
  lift = ListT . fmap (`Cons` empty)

instance Monad m => Alternative (ListT m) where
  empty = ListT (return Nil)
  as <|> bs = ListT $ runListT as >>= \case
    Nil -> runListT bs
    Cons a as' -> return $ Cons a (as' <|> bs)

instance Monad m => MonadPlus (ListT m)

instance Monad m => Semigroup (ListT m a) where
  (<>) = mplus

instance Monad m => Monoid (ListT m a) where
  mempty = empty
  mappend = mplus

instance (MonadIO m) => MonadIO (ListT m) where
  liftIO = lift . liftIO

instance (MonadReader s m) => MonadReader s (ListT m) where
  ask     = lift ask
  local f m = ListT $ fmap (fmap (local f)) . local f $ runListT m

instance (MonadState s m) => MonadState s (ListT m) where
  get = lift get
  put = lift . put

instance (MonadCont m) => MonadCont (ListT m) where
  callCC f = ListT $
    callCC $ \c -> runListT $ f (\a -> ListT $ c (Cons a empty))

instance (MonadError e m) => MonadError e (ListT m) where
  throwError = lift . throwError
  -- I can't really decide between those two possible implementations.
  -- The first one is more like the IO monad works, the second one catches
  -- all possible errors in the list.
  --  ListT m `catchError` h = ListT $ m `catchError` \e -> runListT (h e)
  (m :: ListT m a) `catchError` h = deepCatch m where
    deepCatch :: ListT m a -> ListT m a
    deepCatch xs =
      ListT $ fmap (fmap deepCatch) (runListT xs)
                 `catchError` \e -> runListT (h e)

instance (Monad m) => MonadLogic (ListT m) where
  msplit  = lift . fmap listFToMaybe . runListT
  -- Default 'interleave' is O(n^2) somehow; It's straightforward to
  -- implement by hand.
  interleave m1 m2 =
    ListT $ runListT m1 >>= \case
              Nil        -> runListT m2
              Cons a m1' -> return $ Cons a (interleave m2 m1')
  -- More "fair" >>=
  (>>-) = fairBind

-- | # Other utilities
choose :: (Foldable f, MonadPlus m) => f a -> m a
choose = foldr (\x r -> return x `mplus` r) mzero

chooseM :: (Foldable f, MonadTrans t, Monad m, MonadPlus (t m)) =>
           f (m a) -> t m a
chooseM = foldr (\x r -> lift x `mplus` r) mzero

-- | Lazy version of 'toList' from `ListT (ST s)`.
toListLazily :: (forall s. ListT (ST s) a) -> [a]
toListLazily xs = runST (loop xs)
  where loop as = runListT as >>= \case
          Nil        -> return []
          Cons a as' -> (a :) <$> unsafeInterleaveST (loop as')

fairBind :: (Monad m) => ListT m a -> (a -> ListT m b) -> ListT m b
fairBind mx k = loop mx [] []
  where
    loop ma [] mcs = ListT $
      runListT ma >>= \case
        Nil        -> runListT $ loop' mcs []
        Cons a ma' -> runListT $ loop ma' (k a : mcs) []
    loop ma (mb:mbs) mcs = ListT $
      runListT mb >>= \case
        Nil        -> runListT $ loop ma mbs mcs
        Cons b mb' -> return $ Cons b (loop ma mbs (mb' : mcs))

    loop' [] [] = mzero
    loop' [] mcs = loop' mcs []
    loop' (mb:mbs) mcs = ListT $
      runListT mb >>= \case
        Nil        -> runListT $ loop' mbs mcs
        Cons b mb' -> return $ Cons b (loop' mbs (mb' : mcs))
