{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE UndecidableInstances       #-}
{-# LANGUAGE RankNTypes                 #-}
{-# LANGUAGE LambdaCase                 #-}
{-# LANGUAGE GADTs                      #-}
module LogicT(
  Logic, logic, runLogic,
  observeAll, observeOne, observeMany,

  LogicT(),
  observeAllT, observeOneT, observeManyT,

  hfmap, hpure, uncons, recons,

  toListT, fromListT,

  MonadLogic(..),
  reflect,
  lnot
) where

import           Control.Applicative
import           Control.Monad
import           Control.Monad.Identity
import           Control.Monad.Fail

import           Control.Monad.Except
import           Control.Monad.IO.Class ()
import           Control.Monad.Reader
import           Control.Monad.State

import           Control.Monad.Logic (MonadLogic(..), reflect, lnot)
import           ListT

newtype LogicT m a = LogicT
  { runLogicT :: forall r. (forall b. m b -> (b -> r) -> r) ->
                           (a -> r -> r) -> r -> r
  }

observeAllT :: (Monad m) => LogicT m a -> m [a]
observeAllT ma = runLogicT ma (>>=) (\a as -> fmap (a:) as) (return [])

observeOneT :: (Monad m) => LogicT m a -> m (Maybe a)
observeOneT ma = runLogicT ma (>>=) (\a _ -> return (Just a)) (return Nothing)

observeManyT :: forall m a. (Monad m) => Int -> LogicT m a -> m [a]
observeManyT n ma = runLogicT ma bind sk fk n
  where
    bind :: forall b. m b -> (b -> Int -> m [a]) -> Int -> m [a]
    bind mb f i | i <= 0 = return []
                | otherwise = mb >>= \b -> f b i
    
    sk a rest i | i <= 0    = return []
                | otherwise = fmap (a:) (rest (i-1))
    fk _ = return []

type Logic = LogicT Identity

logic :: (forall r. (a -> r -> r) -> r -> r) -> LogicT m a
logic as = LogicT $ const as

runLogic :: Logic a -> (a -> r -> r) -> r -> r
runLogic ma = runLogicT ma purely
  where
    purely :: Identity b -> (b -> r) -> r
    purely (Identity b) k = k b

observeAll :: Logic a -> [a]
observeAll ma = runLogic ma (:) []

observeOne :: Logic a -> Maybe a
observeOne ma = runLogic ma (\a _ -> Just a) Nothing

observeMany :: Int -> Logic a -> [a]
observeMany n = take n . observeAll

instance Functor (LogicT m) where
  fmap f ma = LogicT $ \bind sk fk -> runLogicT ma bind (sk . f) fk

instance Applicative (LogicT m) where
  pure = return
  (<*>) = ap
  (*>) = (>>)

instance Alternative (LogicT m) where
  empty = mzero
  (<|>) = mplus

instance Monad (LogicT m) where
  return a = LogicT $ \_ sk fk -> sk a fk
  ma >>= k = LogicT $ \bind sk ->
    runLogicT ma bind (\a -> runLogicT (k a) bind sk)

instance MonadFail (LogicT m) where
  fail = const mzero

instance MonadPlus (LogicT m) where
  mzero = LogicT $ \_ _ fk -> fk
  mplus ma ma' = LogicT $ \bind sk fk ->
    runLogicT ma bind sk (runLogicT ma' bind sk fk)

instance MonadTrans LogicT where
  lift = hpure

hfmap :: (forall x. m x -> n x) -> LogicT m y -> LogicT n y
hfmap f my = LogicT $ \bind -> runLogicT my (bind . f)

hpure :: m a -> LogicT m a
hpure ma = LogicT $ \bind sk fk -> ma `bind` (\a -> sk a fk)

instance (Monad m) => MonadLogic (LogicT m) where
  msplit = lift . uncons

  interleave m1 m2 = fromListT $ interleave (toListT m1) (toListT m2)

  ma >>- k = fromListT $ toListT ma >>- (toListT . k)

  ifte cond th el =
    LogicT $ \bind sk fk ->
      let sk' a r _flag = runLogicT (th a) bind sk (r True)
          fk' flag = if flag then fk else runLogicT el bind sk fk
      in runLogicT cond (readingBind bind) sk' fk' False

  once m = LogicT $ \bind sk fk ->
    observeOneT m `bind` \case
      Nothing -> fk
      Just a  -> sk a fk

readingBind :: forall m a r.
  (forall b. m b -> (b -> r) -> r) ->
  (forall c. m c -> (c -> a -> r) -> a -> r)
readingBind bind mc k a = mc `bind` flip k a

uncons :: (Monad m) => LogicT m a -> m (Maybe (a, LogicT m a))
uncons ma = runLogicT ma (>>=) sk fk
  where
    sk a rest = return (Just (a, recons rest))
    fk = return Nothing

recons :: m (Maybe (a, LogicT m a)) -> LogicT m a
recons unconsed = LogicT $ \bind sk fk ->
  unconsed `bind` \case
    Nothing      -> fk
    Just (a, as) -> sk a (runLogicT as bind sk fk)

instance (MonadReader e m) => MonadReader e (LogicT m) where
  ask = lift ask
  local f = hfmap (local f)

instance (MonadState s m) => MonadState s (LogicT m) where
  state = lift . state

instance (MonadIO m) => MonadIO (LogicT m) where
  liftIO = lift . liftIO

-- | LogicT to ListT conversion.
toListT :: forall m a. (Monad m) => LogicT m a -> ListT m a
toListT ma = runLogicT ma bind sk fk
  where bind :: forall b. m b -> (b -> ListT m a) -> ListT m a
        bind = (>>=) . lift
        
        sk :: a -> ListT m a -> ListT m a
        sk a next = ListT $ return $ Cons a next
        
        fk :: ListT m a
        fk = mzero

-- | ListT to LogicT conversion.
fromListT :: ListT m a -> LogicT m a
fromListT m = LogicT $ \bind sk fk ->
  let go (ListT as) = as `bind` \case
        Nil        -> fk
        Cons a as' -> sk a (go as')
  in go m
