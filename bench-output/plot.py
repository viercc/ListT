from subprocess import (Popen, PIPE)
import numpy as np
import matplotlib.pyplot as plt

fname="stats-20210721.csv"
schema = {
  'names':('Name','Monad','N','Time'),
  'formats':('U16','U16','f8','f8')
}
with Popen(['tr', '/', ','], stdin=open(fname,'r'), stdout=PIPE) as p:
    data = np.loadtxt(p.stdout, dtype=schema, delimiter=",", skiprows=1, usecols=range(4))

def uniq(xs):
    ys=[]
    uniqs=set()
    for x in xs:
        if not (x in uniqs):
            ys.append(x)
            uniqs.add(x)
    return ys

all_bench_names=uniq(data['Name'])
all_monad_types=uniq(data['Monad'])

def plot_bench(bench_name, monads=all_monad_types):
    data_b = data[data['Name']==bench_name]
    
    fig, ax=plt.subplots()
    ax.set_title(bench_name)
    ax.set_xlabel('N')
    ax.set_xscale('log')
    ax.set_ylabel('Time')
    ax.set_yscale('log')
    for monad_t in monads:
        data_p = data_b[data_b['Monad'] == monad_t]
        ax.plot('N','Time', marker='s', label=monad_t, data=data_p)
    ax.legend()
    return fig, ax

# plot to images
shown_monads = ['[]', 'ListT_I', 'OrigLogic', 'LSeq']

for bn in all_bench_names:
    fig, ax = plot_bench(bn, monads=shown_monads)
    fig.savefig(bn + '.png', dpi=72)
    plt.close(fig)
